
FROM rust:1.53

RUN apt-get update && \
    apt-get install -y curl libssl-dev pkg-config && \
    rm -rf /var/lib/apt/lists/*

RUN cargo install drill
